using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Character")]
    // референс компонента  CharacterController
    [SerializeField]
    private CharacterController character;

    // Cкорость передвижения
    [SerializeField] private float moveSpeed = 4;

    [Header("Camera")]
    // референс камеры
    [SerializeField]
    private Camera characterCamera;

    // скорость движения камеры
    [SerializeField] private float camSpeed = 1;


    private void Start()
    {
        //Блокировка и скрытие курсора
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }


    private void Update()
    {

        Move();
        CameraRotation();
    }

    private void Move()
    {
        // Get player input for character movement
        var v = Input.GetAxis("Vertical");
        var h = Input.GetAxis("Horizontal");
        
        //создаем вектор для вращения
        var move = new Vector3(h, 0, v);
        move = character.transform.rotation * move;
        // Если длина вектора перемещения больше 1, нормализуйте его.
        if (move.magnitude > 1)
            move = move.normalized;
        // двигаем персонажа
        character.SimpleMove(move * moveSpeed);
    }

    private void CameraRotation()
    {
        // ввод мыши игрока для движения камерой
        var mx = Input.GetAxisRaw("Mouse X");
        var my = Input.GetAxisRaw("Mouse Y");
        character.transform.Rotate(Vector3.up, mx * camSpeed);
        var currentRotationX = characterCamera.transform.localEulerAngles.x;
        currentRotationX += my * camSpeed;
        // ограничиваем движение камеры (-60) - (60) по оси X.
        if (currentRotationX < 180)
        {
            currentRotationX = Mathf.Min(currentRotationX, 60);
        }
        else if (currentRotationX > 180)
        {
            currentRotationX = Mathf.Max(currentRotationX, 300);
        }
        characterCamera.transform.localEulerAngles = new Vector3(currentRotationX, 0, 0);
    }
}