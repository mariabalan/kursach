using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;
using Slider = UnityEngine.UI.Slider;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    
    [Tooltip("Референс скрипта движения")] [SerializeField]
    private PlayerMovement player;
    
    #region objects

    [Tooltip("Стартовая панель")] [SerializeField]
    private GameObject startPanel;

    [Tooltip("Геймплейная панель")] [SerializeField]
    private GameObject gamePlayPlanel;
    
    [Tooltip("Панель победы ")] [SerializeField]
    private GameObject WinPlanel;
    
    [Tooltip("Панель проигрыша ")] [SerializeField]
    private GameObject LosePlanel;
    
    [Tooltip("Эффекты костра")] [SerializeField]
    private GameObject campfire;

    #endregion


    [Tooltip("UI кол-во палок")] [SerializeField] 
    private TextMeshProUGUI stickCountText;
    
    [Tooltip("UI время")] [SerializeField] 
    private TextMeshProUGUI sliderTime;
    
    [Tooltip("Кол-во палок")] [SerializeField] 
    private int stickCount;

    #region slider

    [Tooltip("Слайдер")] [SerializeField] 
    private Slider slider;

    [Tooltip("Слайдер")] [SerializeField] 
    private int timeLeft = 60;

    private float passedtime;// сколько прошло времени

    #endregion

    private bool gameStarted;

    
    
    
    
    
    void Awake()
    {
        CheckInstance();
    }
    
    private void Update()
    {
        if (stickCount == 0)
            Win();
        if(gameStarted)
            Timer();
        if (timeLeft == 0)
            Lose();

    }
    private void CheckInstance()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Win()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        gamePlayPlanel.SetActive(false);
        WinPlanel.SetActive(true);
        campfire.SetActive(true);
        player.enabled = false;
        gameStarted = false;
    }

    private void Lose()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        gamePlayPlanel.SetActive(false);
        LosePlanel.SetActive(true);
        campfire.SetActive(true);
        player.enabled = false;
    }
    
    
    public void StartGame()
    {
        startPanel.SetActive(false); //деактивируем стартовую панель
        gameStarted = true;
        player.enabled = true;//активируем управление
        gamePlayPlanel.SetActive(true);//активируем геймплейную панель
    }

    public void Restart()
    {
        SceneManager.LoadScene(0); //перезагружаем сцену
    }

    public void Quit()
    {
        Application.Quit();//выходим из игры
    }
    public void SetStickCound()
    {
        stickCount--;
        stickCountText.text = stickCount.ToString();
    }

    private void Timer()
    {
        sliderTime.text = "Time left: " + timeLeft;
        slider.value = timeLeft;
        passedtime += 1 * Time.deltaTime;
        if (passedtime >= 1)
        {
            timeLeft -= 1;
            passedtime = 0;
            
        }
    }

}

internal class SerializeField
{
}