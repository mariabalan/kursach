using UnityEngine;
public class TakeObj : MonoBehaviour
{
    
    [SerializeField]
    private Camera characterCamera;
    
    // слот для объекта
    [SerializeField]
    private Transform slot;
    
    // референс объекта в руке
    private PickableItem pickedItem;
    
    //референс луча
    private  RaycastHit hit;

    private void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            // Выпускаем луч из центра экрана
            var ray = characterCamera.ViewportPointToRay(Vector3.one * 0.5f);

            // проверяем на наличие предмета в руке
            // Если нет,то пытаемся подобрать предмет перед игроком
            if (!pickedItem)
            {
                if (Physics.Raycast(ray, out hit, 10f))
                {
                    // проверяем возможно ли поднять объект
                    var pickable = hit.transform.GetComponent<PickableItem>();
                    // если есть класс PickableItem
                    if (pickable)
                    {
                        // берем предмет
                        PickItem(pickable);
                    }
                }
            }
            else
            {
                if (Physics.Raycast(ray, out hit, 10f))
                {
                    // проверяем возможно ли поднять объект
                    var dropable = hit.collider.gameObject.CompareTag("campfire");
                    // если есть класс PickableItem
                    if (dropable)
                    {
                        // берем предмет
                        DropItem(pickedItem);
                    }
                }

            }
        }
    }

    private void PickItem(PickableItem item)
    {
        pickedItem = item;
        // выключаем riggid body 
        item.Rb.isKinematic = true;
        item.Rb.velocity = Vector3.zero;
        item.Rb.angularVelocity = Vector3.zero;
        // делаем слот родителем объекта
        item.transform.SetParent(slot);
        // обнуляем позицию и вращение объекта
        item.transform.localPosition = Vector3.zero;
        item.transform.localEulerAngles = Vector3.zero;
    }
    
    private void DropItem(PickableItem item)
    {
        // удаляем референс
        pickedItem = null;
        //деактивируем объект
        item.gameObject.SetActive(false);
        //обращаемся к UIManger 
        GameManager.Instance.SetStickCound();
    }

}
